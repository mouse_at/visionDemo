//
//  DSFaceViewController.m
//  VisionLearn
//
//  Created by dasen on 2017/7/6.
//  Copyright © 2017年 Dasen. All rights reserved.
//

#import "DSFaceViewController.h"
#import "DSViewTool.h"
#import "DSDetectData.h"
#import <Vision/Vision.h>

#import "GoogLeNetPlaces.h"

@interface DSFaceViewController () < UINavigationControllerDelegate>

@property (nonatomic, assign) DSDetectionType detectionType;

@property (nonatomic, strong) UIImageView *showImageView;
@property (nonatomic, strong) UIImage *showImage;
@property (nonatomic, strong) UILabel *showLabel;
@property (nonatomic, strong) GoogLeNetPlaces *model;

@end

@implementation DSFaceViewController

- (instancetype _Nullable )initWithDetectionType:(DSDetectionType)type
{
    if (self = [super init]) {
        _detectionType = type;
    }
    return self;
}

- (instancetype _Nullable )initWithDetectionType:(DSDetectionType)type image:(UIImage*)image{
    if (self = [super init]) {
        _detectionType = type;
        _showImage = image;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.showImageView];
    self.view.backgroundColor = [UIColor whiteColor];
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [self presentViewController:self.pickerVc animated:NO completion:nil];
//    });
    
    _showLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 320, SCREEN_WIDTH, 400)];
    _showLabel.numberOfLines = 0;
    _showLabel.textAlignment = NSTextAlignmentCenter;
    _showLabel.textColor = [UIColor blackColor];
    [self.view addSubview:_showLabel];
    
    if (_showImage) {
        [self detectFace:_showImage];
    }else{
        [self detectFace:[UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"photo/face/face3.jpg"]]];
    }
    
}

- (CVPixelBufferRef)pixelBufferFromCGImage:(CGImageRef)image{
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], kCVPixelBufferCGImageCompatibilityKey,
                             [NSNumber numberWithBool:YES], kCVPixelBufferCGBitmapContextCompatibilityKey,
                             nil];
    CVPixelBufferRef pxbuffer = NULL;
    CGFloat frameWidth = CGImageGetWidth(image);
    CGFloat frameHeight = CGImageGetHeight(image);
    frameWidth = 224;
    frameHeight = 224;
    CVReturn status = CVPixelBufferCreate(kCFAllocatorDefault, frameWidth, frameHeight, kCVPixelFormatType_32ARGB, (__bridge CFDictionaryRef) options, &pxbuffer);
    NSParameterAssert(status == kCVReturnSuccess && pxbuffer != NULL);
    CVPixelBufferLockBaseAddress(pxbuffer, 0);
    void *pxdata = CVPixelBufferGetBaseAddress(pxbuffer);
    NSParameterAssert(pxdata != NULL);
    CGColorSpaceRef rgbColorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(pxdata, frameWidth, frameHeight, 8, CVPixelBufferGetBytesPerRow(pxbuffer), rgbColorSpace, (CGBitmapInfo)kCGImageAlphaNoneSkipFirst);
    NSParameterAssert(context);
    CGContextConcatCTM(context, CGAffineTransformIdentity);
    CGContextDrawImage(context, CGRectMake(0, 0, frameWidth, frameHeight), image);
    CGColorSpaceRelease(rgbColorSpace);
    CGContextRelease(context);
    CVPixelBufferUnlockBaseAddress(pxbuffer, 0);
    return pxbuffer;
}

- (void)detectFace:(UIImage *)image{
    
    UIImage *localImage = [image scaleImage:SCREEN_WIDTH];
    [self.showImageView setImage:localImage];
    self.showImageView.size = localImage.size;
    
    [DSVisionTool detectImageWithType:self.detectionType image:localImage complete:^(DSDetectData * _Nullable detectData) {
        switch (self.detectionType) {
            case DSDetectionTypeFace:{
                for (NSValue *rectValue in detectData.faceAllRect) {
                    [self.showImageView addSubview: [DSViewTool getRectViewWithFrame:rectValue.CGRectValue]];
                }
                NSMutableString *showString = [[NSMutableString alloc] init];
                for (NSInteger i = 0; i < detectData.faceAllRect.count; i++) {
                    CGRect rect = [detectData.faceAllRect[i] CGRectValue];
                    if (i != 0) {
                        [showString appendString:[NSString stringWithFormat:@"\nx:%.0f,y:%.0f,width:%.0f,height:%.0f",rect.origin.x,rect.origin.y,rect.size.width,rect.size.height]];
                    }else{
                        [showString appendString:[NSString stringWithFormat:@"x:%.0f,y:%.0f,width:%.0f,height:%.0f",rect.origin.x,rect.origin.y,rect.size.width,rect.size.height]];
                    }
                    
                }
                CGRect rect = _showLabel.frame;
                if (localImage.size.height - rect.origin.y >= 0) {
                    rect.origin.y += localImage.size.height - rect.origin.y;
                    _showLabel.frame = rect;
                }
                [DSVisionTool saveMessage:showString WithFileName:@"face"];
                
                _showLabel.text = showString;
            }
                break;
            case DSDetectionTypeLandmark:
                
                for (DSDetectFaceData *faceData in detectData.facePoints) {
                  self.showImageView.image = [DSViewTool drawImage:self.showImageView.image observation:faceData.observation pointArray:faceData.allPoints];
                    NSMutableString *stringM = [NSMutableString string];
                    [DSVisionTool getAllkeyWithClass:[DSDetectFaceData class] isProperty:YES block:^(NSString *key) {
                        // 过滤属性
                        if ([key isEqualToString:@"allPoints"] || [key isEqualToString:@"observation"]) {
                            return;
                        }
                        
                        // 得到对应细节具体特征（鼻子，眼睛。。。）
                        VNFaceLandmarkRegion2D *region2D = [faceData valueForKey:key];
                        
                        // 特征存储对象进行存储
                        NSMutableArray *regionArray = [NSMutableArray array];
                        // 转换特征的所有点
                        for (int i=0; i<region2D.pointCount; i++) {
                            CGPoint point = region2D.normalizedPoints[i];
                            CGFloat rectWidth = self.showImageView.image.size.width * faceData.observation.boundingBox.size.width;
                            CGFloat rectHeight = self.showImageView.image.size.height * faceData.observation.boundingBox.size.height;
                            CGPoint p = CGPointMake(point.x * rectWidth + faceData.observation.boundingBox.origin.x * self.showImageView.image.size.width, faceData.observation.boundingBox.origin.y * self.showImageView.image.size.height + point.y * rectHeight);
                            [regionArray addObject:@{@"x":@(p.x),@"y":@(p.y)}];
                            if (stringM.length == 0) {
                                [stringM appendString:[NSString stringWithFormat:@"key:%@,point.x=%.0f,point.y=%.0f",key,p.x,p.y]];
                            }else{
                                [stringM appendString:[NSString stringWithFormat:@"\nkey:%@,point.x=%.0f,point.y=%.0f",key,p.x,p.y]];
                            }
                        }
                        
                    }];
                    [DSVisionTool saveMessage:stringM WithFileName:@"landmark"];
                    _showLabel.text = stringM;
                }
                
                break;
            case DSDetectionTypeTextRectangles:{
                for (NSValue *rectValue in detectData.textAllRect) {
                    [self.showImageView addSubview: [DSViewTool getRectViewWithFrame:rectValue.CGRectValue]];
                }
                NSMutableString *showString = [[NSMutableString alloc] init];
                for (NSInteger i = 0; i < detectData.textAllRect.count; i++) {
                    CGRect rect = [detectData.textAllRect[i] CGRectValue];
                    if (i != 0) {
                        [showString appendString:[NSString stringWithFormat:@"\nx:%.0f,y:%.0f,width:%.0f,height:%.0f",rect.origin.x,rect.origin.y,rect.size.width,rect.size.height]];
                    }else{
                        [showString appendString:[NSString stringWithFormat:@"x:%.0f,y:%.0f,width:%.0f,height:%.0f",rect.origin.x,rect.origin.y,rect.size.width,rect.size.height]];
                    }
                    
                }
                CGRect rect = _showLabel.frame;
                if (localImage.size.height - rect.origin.y >= 0) {
                    rect.origin.y += localImage.size.height - rect.origin.y;
                    _showLabel.frame = rect;
                }
                [DSVisionTool saveMessage:showString WithFileName:@"rectangles"];
                _showLabel.text = showString;
            }
                break;
            default:
                break;
        }
        
    }];
}

#pragma mark 懒加载控件
- (UIImageView *)showImageView{
    if (!_showImageView) {
        _showImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 64, SCREEN_SIZE.width, SCREEN_SIZE.width)];
        _showImageView.contentMode = UIViewContentModeScaleAspectFit;
        _showImageView.backgroundColor = [UIColor orangeColor];
    }
    return _showImageView;
}

- (GoogLeNetPlaces *)model{
    if (!_model) {
        _model = [[GoogLeNetPlaces alloc] init];
    }
    return _model;
}

@end
