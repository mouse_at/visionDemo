//
//  DSFaceViewController.m
//  VisionLearn
//
//  Created by dasen on 2017/7/6.
//  Copyright © 2017年 Dasen. All rights reserved.
//

#import "BarcodeViewController.h"
#import "DSViewTool.h"
#import "DSDetectData.h"
#import <Vision/Vision.h>

#import "GoogLeNetPlaces.h"

@interface BarcodeViewController () < UINavigationControllerDelegate>

@property (nonatomic, assign) DSDetectionType detectionType;

@property (nonatomic, strong) UIImageView *showImageView;
@property (nonatomic, strong) UIImage *showImage;
@property (nonatomic, strong) UILabel *showLabel;
@property (nonatomic, strong) GoogLeNetPlaces *model;

@end

@implementation BarcodeViewController

- (instancetype _Nullable )initWithDetectionType:(DSDetectionType)type image:(UIImage*)image
{
    if (self = [super init]) {
        _detectionType = type;
        _showImage = image;
    }
    return self;
}

- (instancetype _Nullable )initWithDetectionType:(DSDetectionType)type
{
    if (self = [super init]) {
        _detectionType = type;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.showImageView];
    self.view.backgroundColor = [UIColor whiteColor];
    
    //    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    //        [self presentViewController:self.pickerVc animated:NO completion:nil];
    //    });
    
    
    _showLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 80 + SCREEN_WIDTH, SCREEN_WIDTH, 300)];
    _showLabel.numberOfLines = 0;
    _showLabel.textAlignment = NSTextAlignmentCenter;
    _showLabel.textColor = [UIColor blackColor];
    [self.view addSubview:_showLabel];
    
    [self detectFace:_showImage];
    
}

- (CVPixelBufferRef)pixelBufferFromCGImage:(CGImageRef)image{
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], kCVPixelBufferCGImageCompatibilityKey,
                             [NSNumber numberWithBool:YES], kCVPixelBufferCGBitmapContextCompatibilityKey,
                             nil];
    CVPixelBufferRef pxbuffer = NULL;
    CGFloat frameWidth = CGImageGetWidth(image);
    CGFloat frameHeight = CGImageGetHeight(image);
    frameWidth = 224;
    frameHeight = 224;
    CVReturn status = CVPixelBufferCreate(kCFAllocatorDefault, frameWidth, frameHeight, kCVPixelFormatType_32ARGB, (__bridge CFDictionaryRef) options, &pxbuffer);
    NSParameterAssert(status == kCVReturnSuccess && pxbuffer != NULL);
    CVPixelBufferLockBaseAddress(pxbuffer, 0);
    void *pxdata = CVPixelBufferGetBaseAddress(pxbuffer);
    NSParameterAssert(pxdata != NULL);
    CGColorSpaceRef rgbColorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(pxdata, frameWidth, frameHeight, 8, CVPixelBufferGetBytesPerRow(pxbuffer), rgbColorSpace, (CGBitmapInfo)kCGImageAlphaNoneSkipFirst);
    NSParameterAssert(context);
    CGContextConcatCTM(context, CGAffineTransformIdentity);
    CGContextDrawImage(context, CGRectMake(0, 0, frameWidth, frameHeight), image);
    CGColorSpaceRelease(rgbColorSpace);
    CGContextRelease(context);
    CVPixelBufferUnlockBaseAddress(pxbuffer, 0);
    return pxbuffer;
}


- (void)predictionWithImage:(CIImage * )image
{
    //两种初始化方法均可
    //    Resnet50* resnet50 = [[Resnet50 alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Resnet50" ofType:@"mlmodelc"]] error:nil];
    
    NSError *error = nil;
    //创建VNCoreMLModel
    VNCoreMLModel *vnCoreMMModel = [VNCoreMLModel modelForMLModel:self.model.model error:&error];
    
    // 创建处理requestHandler
    VNImageRequestHandler *handler = [[VNImageRequestHandler alloc] initWithCIImage:image options:@{}];
    VNDetectRectanglesRequest *request2 = [[VNDetectRectanglesRequest alloc] initWithCompletionHandler:^(VNRequest * _Nonnull request, NSError * _Nullable error) {
        CGFloat confidence = 0.0f;
        
        VNClassificationObservation *tempClassification = nil;
        
        for (VNClassificationObservation *classification in request.results) {
            if (classification.confidence > confidence) {
                confidence = classification.confidence;
                tempClassification = classification;
            }
        }
        NSLog(@"识别结果:%@,匹配率:%.2f",tempClassification.identifier,tempClassification.confidence);
    }];
    NSLog(@" 打印信息:%@",handler);
    // 创建request
    VNCoreMLRequest *request = [[VNCoreMLRequest alloc] initWithModel:vnCoreMMModel completionHandler:^(VNRequest * _Nonnull request, NSError * _Nullable error) {
        
        CGFloat confidence = 0.0f;
        
        VNClassificationObservation *tempClassification = nil;
        
        for (VNClassificationObservation *classification in request.results) {
            if (classification.confidence > confidence) {
                confidence = classification.confidence;
                tempClassification = classification;
            }
        }
        NSLog(@"识别结果:%@,匹配率:%.2f",tempClassification.identifier,tempClassification.confidence);
    }];
    
    // 发送识别请求
    //    [handler performRequests:@[request] error:&error];
    [handler performRequests:@[request2] error:&error];
    if (error) {
        NSLog(@"%@",error.localizedDescription);
    }
}

- (void)predictionWithCGImage:(CGImageRef )image
{
    //两种初始化方法均可
    //    Resnet50* resnet50 = [[Resnet50 alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Resnet50" ofType:@"mlmodelc"]] error:nil];
    
    NSError *error = nil;
    //创建VNCoreMLModel
    VNCoreMLModel *vnCoreMMModel = [VNCoreMLModel modelForMLModel:self.model.model error:&error];
    
    // 创建处理requestHandler
    VNSequenceRequestHandler *handler = [[VNSequenceRequestHandler alloc] init];
    
    
    NSLog(@" 打印信息:%@",handler);
    // 创建request
    VNCoreMLRequest *request = [[VNCoreMLRequest alloc] initWithModel:vnCoreMMModel completionHandler:^(VNRequest * _Nonnull request, NSError * _Nullable error) {
        
        CGFloat confidence = 0.0f;
        
        VNClassificationObservation *tempClassification = nil;
        
        for (VNClassificationObservation *classification in request.results) {
            if (classification.confidence > confidence) {
                confidence = classification.confidence;
                tempClassification = classification;
            }
        }
        NSLog(@"识别结果:%@,匹配率:%.2f",tempClassification.identifier,tempClassification.confidence);
    }];
    
    // 发送识别请求
    //    [handler performRequests:@[request] error:&error];
    [handler performRequests:@[request] onCGImage:image error:nil];
    if (error) {
        NSLog(@"%@",error.localizedDescription);
    }
}

- (void)detectFace:(UIImage *)image{
    
    UIImage *localImage = [image scaleImage:SCREEN_WIDTH];
    [self.showImageView setImage:localImage];
//    self.showImageView.size = localImage.size;
    
    [DSVisionTool detectImageWithType:self.detectionType image:localImage complete:^(DSDetectData * _Nullable detectData) {
        switch (self.detectionType) {
            case DSDetectionTypeBarcode:{
                NSMutableString *showString = [[NSMutableString alloc] init];
                for (NSInteger i = 0; i < detectData.barcodeStrings.count; i++) {
                    NSDictionary *dic = detectData.barcodeStrings[i];
                    if (i != 0) {
                        [showString appendString:[NSString stringWithFormat:@"\ntype:%@,payloadStringValue:%@",dic[@"type"],dic[@"payloadStringValue"]]];
                    }else{
                        [showString appendString:[NSString stringWithFormat:@"type:%@,payloadStringValue:%@",dic[@"type"],dic[@"payloadStringValue"]]];
                    }
                   
                }
                [DSVisionTool saveMessage:showString WithFileName:@"barcode"];
                _showLabel.text = showString;
                break;
            }
            default:
                break;
        }
        
    }];
}

#pragma mark 懒加载控件
- (UIImageView *)showImageView{
    if (!_showImageView) {
        _showImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 64, SCREEN_SIZE.width, SCREEN_SIZE.width)];
        _showImageView.contentMode = UIViewContentModeScaleAspectFit;
//        _showImageView.backgroundColor = [UIColor orangeColor];
    }
    return _showImageView;
}

- (GoogLeNetPlaces *)model{
    if (!_model) {
        _model = [[GoogLeNetPlaces alloc] init];
    }
    return _model;
}

@end

