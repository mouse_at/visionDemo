//
//  BarcodeViewController.h
//  VisionLearn
//
//  Created by herong on 2018/10/22.
//  Copyright © 2018年 Dasen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DSVisionTool.h"
@interface BarcodeViewController : UIViewController
- (instancetype _Nullable )initWithDetectionType:(DSDetectionType)type image:(UIImage*)image;

- (instancetype _Nullable )initWithDetectionType:(DSDetectionType)type;
@end
