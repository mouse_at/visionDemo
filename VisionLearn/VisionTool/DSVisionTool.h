//
//  DSVisionTool.h
//  DSVisionTool
//
//  Created by dasen on 2017/7/5.
//  Copyright © 2017年 Dasen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DSDetectData.h"
#import "DSViewTool.h"
#import <AVFoundation/AVFoundation.h>
typedef void(^detectImageHandler)(DSDetectData * __nullable detectData);

typedef NS_ENUM(NSUInteger,DSDetectionType) {
    DSDetectionTypeFace,     // 人脸识别
    DSDetectionTypeLandmark, // 特征识别
    DSDetectionTypeBarcode, // 二维码识别
    DSDetectionTypeTextRectangles,  // 文字识别
    DSDetectionTypeFaceHat,
    DSDetectionTypeFaceRectangles
};

@interface DSVisionTool : NSObject

+ (instancetype)sharedTool;

/// 转换坐标与大小
+ (CGRect)convertRect:(CGRect)oldRect imageSize:(CGSize)imageSize;

/// 识别图片
+ (void)detectImageWithType:(DSDetectionType)type image:(UIImage *_Nullable)image complete:(detectImageHandler _Nullable )complete;

// 获取对象属性keys
+ (NSArray *)getAllkeyWithClass:(Class)class isProperty:(BOOL)property block:(void(^)(NSString *key))block;

// 获取对象特征信息的返回值
+ (NSString *)getReturnMessageWithDetectionType:(DSDetectionType)type data:(DSDetectData*)data image:(UIImage*)image;

// 批处理扫描文件
+ (void)detectObjectInFileWithDSDetectType:(DSDetectionType)type;

// 保存到文件
+ (void)saveMessage:(id)message WithFileName:(NSString*)fileName;

@end
