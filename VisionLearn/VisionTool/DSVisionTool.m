//
//  DSVisionTool.m
//  DSVisionTool
//
//  Created by dasen on 2017/7/5.
//  Copyright © 2017年 Dasen. All rights reserved.
//
#import <objc/runtime.h>

#import <Vision/Vision.h>
#import <AVFoundation/AVFoundation.h>
#import "DSDetectData.h"
#import "DSVisionTool.h"
#import "HRImage.h"

typedef void(^CompletionHandler)(VNRequest * _Nullable request, NSError * _Nullable error);

@interface DSVisionTool()

@end

@implementation DSVisionTool

/// 识别图片
+ (void)detectImageWithType:(DSDetectionType)type image:(UIImage *_Nullable)image complete:(detectImageHandler _Nullable )complete
{
    // 转换CIImage
    CIImage *convertImage = [[CIImage alloc]initWithImage:image];
    
    // 创建处理requestHandler
    VNImageRequestHandler *detectRequestHandler = [[VNImageRequestHandler alloc]initWithCIImage:convertImage options:@{}];
    
    // 创建BaseRequest
    VNImageBasedRequest *detectRequest = [[VNImageBasedRequest alloc]init];
    
    // 设置回调
    CompletionHandler completionHandler = ^(VNRequest *request, NSError * _Nullable error) {
        NSArray *observations = request.results;
        [self handleImageWithType:type image:image observations:observations complete:complete];
    };

    switch (type) {
        case DSDetectionTypeFace:
            detectRequest =  [[VNDetectFaceRectanglesRequest alloc]initWithCompletionHandler:completionHandler];
            break;
        case DSDetectionTypeLandmark:
            detectRequest = [[VNDetectFaceLandmarksRequest alloc]initWithCompletionHandler:completionHandler];
            break;
        case DSDetectionTypeTextRectangles:
            detectRequest = [[VNDetectTextRectanglesRequest alloc]initWithCompletionHandler:completionHandler];
            [detectRequest setValue:@(YES) forKey:@"reportCharacterBoxes"]; // 设置识别具体文字
            break;
        case DSDetectionTypeBarcode:
            detectRequest = [[VNDetectBarcodesRequest alloc] initWithCompletionHandler:completionHandler];
            break;
            
        default:
            break;
    }
    
    // 发送识别请求
    [detectRequestHandler performRequests:@[detectRequest] error:nil];
}

/// 处理识别数据
+ (void)handleImageWithType:(DSDetectionType)type image:(UIImage *_Nullable)image observations:(NSArray *)observations complete:(detectImageHandler _Nullable )complete{
        switch (type) {
            case DSDetectionTypeFace:
                [self faceRectangles:observations image:image complete:complete];
                break;
                
            case DSDetectionTypeLandmark:
            {
                [self faceLandmarks:observations image:image complete:complete];
                break;
            }
            case DSDetectionTypeTextRectangles:
            {
                [self textRectangles:observations image:image complete:complete];
                break;
            }
            case DSDetectionTypeBarcode:
            {
                [self barcode:observations image:image complete:complete];
                break;
            }
            default:
                break;
        }
}

// 处理文本回调
+ (void)textRectangles:(NSArray *)observations image:(UIImage *_Nullable)image complete:(detectImageHandler _Nullable )complete{
    
    NSMutableArray *tempArray = @[].mutableCopy;
    DSDetectData *detectTextData = [[DSDetectData alloc]init];
    
    for (VNTextObservation *observation  in observations) {
        for (VNRectangleObservation *box in observation.characterBoxes) {
            // CreateBoxView
            NSValue *ractValue = [NSValue valueWithCGRect:[self convertRect:box.boundingBox imageSize:image.size]];
            [tempArray addObject:ractValue];
        }
    }
    detectTextData.textAllRect = tempArray;
    if (complete) {
        complete(detectTextData);
    }
}

// 处理人脸识别回调
+ (void)faceRectangles:(NSArray *)observations image:(UIImage *_Nullable)image complete:(detectImageHandler _Nullable )complete{
    
    NSMutableArray *tempArray = @[].mutableCopy;
    
    DSDetectData *detectFaceData = [[DSDetectData alloc]init];
    for (VNFaceObservation *observation  in observations) {
        NSValue *ractValue = [NSValue valueWithCGRect:[self convertRect:observation.boundingBox imageSize:image.size]];
        [tempArray addObject:ractValue];
    }
    
    detectFaceData.faceAllRect = tempArray;
    if (complete) {
        complete(detectFaceData);
    }
}

// 处理人脸特征回调
+ (void)faceLandmarks:(NSArray *)observations image:(UIImage *_Nullable)image complete:(detectImageHandler _Nullable )complete{
    
    DSDetectData *detectData = [[DSDetectData alloc]init];

    for (VNFaceObservation *observation  in observations) {
        
        // 创建特征存储对象
        DSDetectFaceData *detectFaceData = [[DSDetectFaceData alloc]init];
        
        // 获取细节特征
        VNFaceLandmarks2D *landmarks = observation.landmarks;
        
        [self getAllkeyWithClass:[VNFaceLandmarks2D class] isProperty:YES block:^(NSString *key) {
           // 过滤属性
           if ([key isEqualToString:@"allPoints"]) {
               return;
           }
            
           // 得到对应细节具体特征（鼻子，眼睛。。。）
           VNFaceLandmarkRegion2D *region2D = [landmarks valueForKey:key];
            
           // 特征存储对象进行存储
           [detectFaceData setValue:region2D forKey:key];
           [detectFaceData.allPoints addObject:region2D];
        }];
        
        detectFaceData.observation = observation;
        [detectData.facePoints addObject:detectFaceData];
    }
    if (complete) {
        complete(detectData);
    }
}

// 处理barcode回调
+ (void)barcode:(NSArray *)observations image:(UIImage *_Nullable)image complete:(detectImageHandler _Nullable )complete{
    
    DSDetectData *detectData = [[DSDetectData alloc]init];
    
    for (VNBarcodeObservation *observation  in observations) {
        if (observation.payloadStringValue) {
            [detectData.barcodeStrings addObject:@{@"payloadStringValue":observation.payloadStringValue,@"type":observation.symbology}];
        }
    }
    if (complete) {
        complete(detectData);
    }
}

/// 转换Rect
+ (CGRect)convertRect:(CGRect)oldRect imageSize:(CGSize)imageSize{
    
    CGFloat w = oldRect.size.width * imageSize.width;
    CGFloat h = oldRect.size.height * imageSize.height;
    CGFloat x = oldRect.origin.x * imageSize.width;
    CGFloat y = imageSize.height - (oldRect.origin.y * imageSize.height) - h;
    
    return CGRectMake(x, y, w, h);
}

// 获取对象属性keys
+ (NSArray *)getAllkeyWithClass:(Class)class isProperty:(BOOL)property block:(void(^)(NSString *key))block{
    
    NSMutableArray *keys = @[].mutableCopy;
    unsigned int outCount = 0;
    
    Ivar *vars = NULL;
    objc_property_t *propertys = NULL;
    const char *name;
    
    if (property) {
        propertys = class_copyPropertyList(class, &outCount);
    }else{
        vars = class_copyIvarList(class, &outCount);
    }
    
    for (int i = 0; i < outCount; i ++) {
        
        if (property) {
            objc_property_t property = propertys[i];
            name = property_getName(property);
        }else{
            Ivar var = vars[i];
            name = ivar_getName(var);
        }
        
        NSString *key = [NSString stringWithUTF8String:name];
        block(key);
    }
    free(vars);
    return keys.copy;
}

+ (void)detectObjectInFileWithDSDetectType:(DSDetectionType)type{
    NSString* path;
    switch (type) {
        case DSDetectionTypeFace:{
            path = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"photo/face"];
        }
            break;
        case DSDetectionTypeLandmark:{
            path = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"photo/face"];
        }
            break;
        case DSDetectionTypeBarcode:{
            path = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"photo/barcode"];
        }
            break;
        case DSDetectionTypeTextRectangles:{
            path = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"photo/textRectangles"];
        }
            break;
            
        default:
            break;
    }
    NSFileManager *man = [NSFileManager defaultManager];
    NSArray *fileArray = [man contentsOfDirectoryAtPath:path error:nil];
    NSMutableString *stringM = [NSMutableString string];
    for (NSInteger i = 0; i < fileArray.count; i++) {
        NSString *subPath = [path stringByAppendingPathComponent:fileArray[i]];
        HRImage *image = [[HRImage alloc] initWithContentsOfFile:subPath];
        image.path = [subPath lastPathComponent];
        [image setValue:[subPath lastPathComponent] forKey:@"path"];
        [self detectImageWithType:type image:image complete:^(DSDetectData * _Nullable detectData) {
            NSString*message = [self getReturnMessageWithDetectionType:type data:detectData image:image];
            [stringM appendFormat:@"%@---------------%@\n",image.path,message];
        }];
    }
    [self saveMessage:stringM WithFileName: [path lastPathComponent]];
    
}

// 获取对象特征信息的返回值
+ (NSString *)getReturnMessageWithDetectionType:(DSDetectionType)type data:(DSDetectData*)data image:(UIImage*)image{
    NSMutableString *returnString = [NSMutableString string];
    switch (type) {
        case DSDetectionTypeFace:{
            for (NSInteger i = 0; i < data.faceAllRect.count; i++) {
                CGRect rect = [data.faceAllRect[i] CGRectValue];
                if (i != 0) {
                    [returnString appendString:[NSString stringWithFormat:@"\nx:%.0f,y:%.0f,width:%.0f,height:%.0f",rect.origin.x,rect.origin.y,rect.size.width,rect.size.height]];
                }else{
                    [returnString appendString:[NSString stringWithFormat:@"x:%.0f,y:%.0f,width:%.0f,height:%.0f",rect.origin.x,rect.origin.y,rect.size.width,rect.size.height]];
                }
                
            }
        }
            break;
        case DSDetectionTypeLandmark:{
            for (DSDetectFaceData *faceData in data.facePoints) {
                NSMutableString *stringM = [NSMutableString string];
                [DSVisionTool getAllkeyWithClass:[DSDetectFaceData class] isProperty:YES block:^(NSString *key) {
                    // 过滤属性
                    if ([key isEqualToString:@"allPoints"] || [key isEqualToString:@"observation"]) {
                        return;
                    }
                    
                    // 得到对应细节具体特征（鼻子，眼睛。。。）
                    VNFaceLandmarkRegion2D *region2D = [faceData valueForKey:key];
                    
                    // 特征存储对象进行存储
                    NSMutableArray *regionArray = [NSMutableArray array];
                    // 转换特征的所有点
                    for (int i=0; i<region2D.pointCount; i++) {
                        CGPoint point = region2D.normalizedPoints[i];
                        CGFloat rectWidth = image.size.width * faceData.observation.boundingBox.size.width;
                        CGFloat rectHeight = image.size.height * faceData.observation.boundingBox.size.height;
                        CGPoint p = CGPointMake(point.x * rectWidth + faceData.observation.boundingBox.origin.x * image.size.width, faceData.observation.boundingBox.origin.y * image.size.height + point.y * rectHeight);
                        [regionArray addObject:@{@"x":@(p.x),@"y":@(p.y)}];
                        if (stringM.length == 0) {
                            [stringM appendString:[NSString stringWithFormat:@"key:%@,point.x=%.0f,point.y=%.0f",key,p.x,p.y]];
                        }else{
                            [stringM appendString:[NSString stringWithFormat:@"\nkey:%@,point.x=%.0f,point.y=%.0f",key,p.x,p.y]];
                        }
                    }
                    
                }];
                [returnString appendString:stringM];
            }
            
        }
            break;
        case DSDetectionTypeTextRectangles:{
            for (NSInteger i = 0; i < data.textAllRect.count; i++) {
                CGRect rect = [data.textAllRect[i] CGRectValue];
                if (i != 0) {
                    [returnString appendString:[NSString stringWithFormat:@"\nx:%.0f,y:%.0f,width:%.0f,height:%.0f",rect.origin.x,rect.origin.y,rect.size.width,rect.size.height]];
                }else{
                    [returnString appendString:[NSString stringWithFormat:@"x:%.0f,y:%.0f,width:%.0f,height:%.0f",rect.origin.x,rect.origin.y,rect.size.width,rect.size.height]];
                }
            }
        }
            break;
        case DSDetectionTypeBarcode:{
            for (NSInteger i = 0; i < data.barcodeStrings.count; i++) {
                NSDictionary *dic = data.barcodeStrings[i];
                if (i != 0) {
                    [returnString appendString:[NSString stringWithFormat:@"\ntype:%@,payloadStringValue:%@",dic[@"type"],dic[@"payloadStringValue"]]];
                }else{
                    [returnString appendString:[NSString stringWithFormat:@"type:%@,payloadStringValue:%@",dic[@"type"],dic[@"payloadStringValue"]]];
                }
                
            }
        }
            break;
            
        default:
            break;
    }
    return returnString;
}


+ (void)saveMessage:(id)message WithFileName:(NSString*)fileName{
    if(fileName && ![fileName isEqualToString:@""]){
        NSDate* date = [NSDate dateWithTimeIntervalSinceNow:0];//获取当前时间0秒后的时间
        NSTimeInterval time= [date timeIntervalSince1970]*1000;// *1000 是精确到毫秒，不乘就是精确到秒
        NSString *timeString = [NSString stringWithFormat:@"%.0f", time];
        NSString *path = [NSString stringWithFormat:@"%@/Documents/%@_%@.txt",NSHomeDirectory(),timeString,fileName];
        NSLog(@"path =  %@",path);
        NSFileHandle *handle = [NSFileHandle fileHandleForWritingAtPath:path];
        if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
            [handle seekToEndOfFile];
            [handle writeData:[[NSData alloc] initWithBase64EncodedString:message options:NSDataBase64DecodingIgnoreUnknownCharacters]];
        }else{
            [message writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
        }
        
    }
}



@end
