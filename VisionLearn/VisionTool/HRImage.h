//
//  HRImage.h
//  VisionLearn
//
//  Created by herong on 2018/10/26.
//  Copyright © 2018年 Dasen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HRImage : UIImage

@property(nonatomic,copy) NSString *path;

@end
